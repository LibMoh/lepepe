import requests
import json
def returndata(loc):
	url = "https://www.metaweather.com/api/location/search/?query="+loc
	r = requests.get(url)
	obj = json.loads(r.text)
	id =  obj[0]['woeid']

	url2 = "https://www.metaweather.com/api/location/"+str(id)
	r2 = requests.get(url2)
	obj2 = json.loads(r2.text)
	return obj2
